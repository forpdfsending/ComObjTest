﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TestCom
{
    [Guid("9E5E5FB2-219D-4ee7-AB27-E4DBED8E123E")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ProgId("Test9.COMINT")]
    public class TestComClass
    {
        public void Init(string userid, string password)
        {
            MessageBox.Show(string.Format("{0}/{1}", userid, password));
        }

        public object Init2()
        {
            //string[] a =new string[3];
            //a[0] = "a";
            //a[1] = "b";
            //a[2] = "c";

            //return a;]

            List<object> list = new List<object>();
            list.Add("aaa");
            list.Add("bbb");
            return list.ToArray();
        }
    }
}